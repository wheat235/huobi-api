package com.maizi.entity;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class Details {

	private BigDecimal amount;

	private BigDecimal open;

	private BigDecimal close;

	private String high;

	private String ts;

	private String id;

	private String count;

	private String low;

	private BigDecimal vol;

}
