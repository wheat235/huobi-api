package com.maizi.http;

import java.util.HashMap;
import java.util.Map;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.maizi.entity.Details;
import com.maizi.entity.HuobiResponse;
import com.maizi.utils.HttpClient;

/**
 * @ClassName: HuobiApiClient
 * @Description: 火币API工具
 * @author 麦子 wheat235@gmail.com
 * @date 2018年3月30日 下午4:03:37
 */
public class HuobiApiClient {

	static final String API_URL = "https://api.huobipro.com/market";

	/**
	 * GET /market/detail 获取 Market Detail 24小时成交量数据
	 */
	public static HuobiResponse<Details> detail(String symbol) {

		Map<String, String> params = new HashMap<String, String>();
		params.put("symbol", symbol);

		String result = HttpClient.doGet(API_URL + "/detail", params);
		return JSON.parseObject(result, new TypeReference<HuobiResponse<Details>>() {});
	}

	public static void main(String[] args) {

		detail("ethusdt");
	}
}
