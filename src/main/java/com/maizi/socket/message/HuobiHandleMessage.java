package com.maizi.socket.message;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.TextMessage;

import com.alibaba.fastjson.JSONObject;
import com.maizi.entity.HuobiResponse;

@SuppressWarnings("rawtypes")
public class HuobiHandleMessage implements IHandleMessage {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@SuppressWarnings("unused")
	private Class clazz;
	
	public HuobiHandleMessage(Class<HuobiResponse> clazz) {
		this.clazz = clazz;
	}
	
	@Override
	public List<TextMessage> subscribes() {
		
		List<TextMessage> subscribes = new ArrayList<TextMessage>();
		
		// 比特币
		subscribes.add(new TextMessage("{\"sub\":\"market.btcusdt.detail\",\"id\": \"id1\"}"));
		
		// 以太坊
		subscribes.add(new TextMessage("{\"sub\":\"market.ethusdt.detail\",\"id\": \"id1\"}"));
		
		// 以太币
		subscribes.add(new TextMessage("{\"sub\":\"market.etcusdt.detail\",\"id\": \"id1\"}"));
		
		// 莱比特
		subscribes.add(new TextMessage("{\"sub\":\"market.ltcusdt.detail\",\"id\": \"id1\"}"));
		
		// 比特币现金
		subscribes.add(new TextMessage("{\"sub\":\"market.bchusdt.detail\",\"id\": \"id1\"}"));
		
		return subscribes;
	}
	
	@Override
	public void subscribeCallback(String message) {
		
		JSONObject jsonObject = JSONObject.parseObject(message);
		
		// 订阅失败
		if ("error".equals(jsonObject.getString("status"))) {
			log.error("订阅火币WebSockets失败:{}", message);
			
			// 订阅成功时
		} else {
			log.info("订阅火币WebSockets成功:{}", message);
		}
	}
	
	@Override
	public void webSocketError(Throwable exception) {
		log.error("火币WebSockets异常", exception);
	}
	
	@Override
	public void afterConnectionClosed() {
		log.error("火币WebSockets连接关闭");
	}
	
	/**
	 * 接收消息处理
	 */
	@Override
	public void call(String message) {
		log.info("接受消息:{}", message);
	}
}
