package com.maizi.socket;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.WebSocketConnectionManager;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;

import com.maizi.entity.HuobiResponse;
import com.maizi.socket.handle.HuobiWebSocketHandler;
import com.maizi.socket.message.HuobiHandleMessage;
import com.maizi.socket.message.IHandleMessage;

@Configuration
public class HuobiWebSocketConfig {

	private String HUOBI_WEB_SOCKET_HOST = "wss://api.huobipro.com/ws";

	@Bean
	@ConditionalOnMissingBean
	public WebSocketConnectionManager wsConnectionManager(WebSocketClient client, WebSocketHandler handler) {

		WebSocketConnectionManager manager = new WebSocketConnectionManager(client, handler, HUOBI_WEB_SOCKET_HOST);
		manager.setAutoStartup(true);
		return manager;
	}

	@Bean
	@ConditionalOnMissingBean
	public WebSocketClient client() {

		return new StandardWebSocketClient();
	}

	@Bean
	@ConditionalOnMissingBean
	public WebSocketHandler handler() {

		return new HuobiWebSocketHandler(handleMessage());
	}

	@Bean
	@ConditionalOnMissingBean
	public IHandleMessage handleMessage() {

		return new HuobiHandleMessage(HuobiResponse.class);
	}

}
